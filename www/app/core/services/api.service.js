angular.module('app.core')

.factory('Api', Api);

function Api ($http, ApiEndpoint, $state, $ionicLoading) {
  return {
    signupWithUsernameAndPassword: function(user, callback) {
      $http({
          method: 'POST',
          url: ApiEndpoint.url + '/user/create',
          data: user,
          headers: {'Content-Type': 'application/json'}
      }).then(function(data) {
          callback(data.data);
        }).catch(function(error){
          var message = error.data;
          if(message.indexOf('Integrity constraint violation: 1062 Duplicate entry') != -1 && message.indexOf('for key \'username\'') != -1){
            alert('Le pseudonyme utilisé existe déjà.');
          }
          if(message.indexOf('Integrity constraint violation: 1062 Duplicate entry') != -1 && message.indexOf('for key \'email\'') != -1){
            alert('L\'email utilisé existe déjà.');
          }
        });
    },
    getApiDataUser: function(id, callback) {
      $http.get(ApiEndpoint.url + '/user/' + id)
        .then(function(data) {
          callback(data['data']);
        }).catch(function(error){
          alert(error.data);
        });
    },
    updateUserData: function(user, callback) {
      $http({
          method: 'PUT',
          url: ApiEndpoint.url + '/user/update/' + user.id,
          data: user,
          headers: {'Content-Type': 'application/json'}
      }).then(function(data) {
          console.log(data);
          //On sauvegarde en local le nouvel utilisateur et redirection dashboard
        }).catch(function(error){
          alert(error);
        });
    },
    deleteUser: function(id) {
      $http.delete(ApiEndpoint.url + '/user/delete/' + id)
        .then(function(data) {
          console.log(data['data']);
          callback(data['data']);
        }).catch(function(error){
          alert(error);
        });
    },
    loginWithUsernameAndPassword: function(data, callback) {
      $http({
          method: 'POST',
          url: ApiEndpoint.url + '/login',
          data: data,
          headers: {'Content-Type': 'application/json'}
      }).then(function(data) {
          callback(data.data);
        }).catch(function(error){
          console.log(error);
          alert(error.data);
        });
    },
    addNewAdvert: function(advert, callback) {
      $http({
          method: 'POST',
          url: ApiEndpoint.url + '/advert/add',
          data: advert,
          headers: {'Content-Type': 'application/json'}
      }).then(function(data) {
        callback(data);
      }).catch(function(error){
        alert(JSON.stringify(error));
      });
    },
    getRelevantAdverts: function(data, callback) {
      var lat = data.lat ? data.lat : null;
      var lng = data.lng ? data.lng : null;
      var distance = data.distance ? data.distance : null;
      var age = data.age ? data.age : null;
      var sex = data.sex ? data.sex : null;
      var vaccinated = data.vaccinated ? data.vaccinated : null;
      var tattooed = data.tattooed ? data.tattooed : null;

      $http.get(ApiEndpoint.url + '/relevant?' +
        'lat=' + lat +
        '&lng=' + lng +
        '&distance=' + distance +
        '&age=' + age +
        '&sex=' + sex +
        '&vaccinated=' + vaccinated +
        '&tattooed=' + tattooed
      )
        .then(function(data) {
          callback(data['data']);
        }).catch(function(error){
          console.log(error.data);
          callback(null);
          $ionicLoading.hide();
        });
    },
    getFavoriteAdverts: function(id, callback) {
      $http.get(ApiEndpoint.url + '/favorites/' + id)
        .then(function(data) {
          callback(data['data']);
        }).catch(function(error){
          console.log(error.data);
          callback(null);
        });
    },
    addToFavorites : function(data, callback) {
      $http({
          method: 'POST',
          url: ApiEndpoint.url + '/favorites/add',
          data: data,
          headers: {'Content-Type': 'application/json'}
      }).then(function(data) {
          callback(data);
        }).catch(function(error){
          if(error.data.indexOf('Integrity constraint violation: 1062 Duplicate entry') != -1 && error.data.indexOf('for key \'PRIMARY\'') != -1){
            alert('L\'annonce est déjà dans vos favoris');
          }
        });
    },
    sendMessage: function(data, callback){
      $http({
        method: 'POST',
        url: ApiEndpoint.url + '/send',
        data: data,
        headers: {'Content-Type': 'application/json'}
      }).then(function(data) {
        callback(data);
      }).catch(function(error){
        alert(JSON.stringify(error));
      });
    },
    getMessages: function(user, callback){
      $http.get(ApiEndpoint.url + '/receive/' + user)
      .then(function(data) {
        callback(data['data']);
      }).catch(function(error){
        console.log(error.data);
        callback(null);
      });
    },
    getSendMessages: function(user, callback){
      $http.get(ApiEndpoint.url + '/send-messages/' + user)
      .then(function(data) {
        callback(data['data']);
      }).catch(function(error){
        console.log(error.data);
        callback(null);
      });
    }
  };
}
