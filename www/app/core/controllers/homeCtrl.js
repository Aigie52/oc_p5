(function(){
  'use strict';

  angular
    .module('app.core')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['$scope', '$rootScope'];

  function HomeCtrl($scope, $rootScope){
    $scope.role = $rootScope.user.role;
  }
})();
