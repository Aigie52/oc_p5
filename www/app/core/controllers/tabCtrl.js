(function(){
  'use strict';

  angular
    .module('app.core')
    .controller('TabCtrl', TabCtrl);

  TabCtrl.$inject = ['$rootScope', '$scope'];

  function TabCtrl($scope, $rootScope){
    if($rootScope.user.role === 'owner') {
      $scope.isOwner = true;
    } else {
       $scope.isOwner = false;
    }
  }
})();
