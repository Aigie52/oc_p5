(function(){
  'use strict';

  angular
    .module('app.core', [
      'ionic',
      'ion-gallery',
    ]);

    angular
      .module('app.core')
      .config(function(ionGalleryConfigProvider) {
        ionGalleryConfigProvider.setGalleryConfig({
          action_label: 'Close',
          toggle: false,
          row_size: 3,
          fixed_row_size: true
        });
      })
      .run(['$ionicPlatform',
         function($ionicPlatform) {
            $ionicPlatform.ready(function() {
              if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
              }
              if(window.StatusBar) {
                StatusBar.styleDefault();
              }
            });
        }])
      .run(function($rootScope) {
          $rootScope.user = {};
        });
})();
