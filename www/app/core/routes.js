(function(){
  'use strict';

  angular
    .module('app.core')
    .config(['$stateProvider', '$urlRouterProvider',
      function($stateProvider, $urlRouterProvider) {

        $stateProvider

        .state('login', {
          url: '/login',
          templateUrl: 'app/users/views/login.html',
          controller: 'LoginCtrl'
        })
        .state('signup', {
          url: '/signup',
          templateUrl: 'app/users/views/signup.html',
          controller: 'SignupCtrl'
        })
        .state('addAdvert', {
          url: '/add-advert',
          templateUrl: 'app/adverts/views/addAdvert.html',
          controller: 'AddAdvertCtrl'
        })

        .state('tab', {
          url: '/tab',
          abstract: true,
          templateUrl: 'app/core/views/tabs.html',
          controller: 'TabCtrl'
        })
        .state('tab.home', {
          url: '/home',
          views: {
            'tab-home': {
              templateUrl: 'app/core/views/home.html',
              controller: 'HomeCtrl'
            }
          }
        })
        .state('tab.profile', {
          url: '/profile',
          views: {
            'tab-profile': {
              templateUrl: 'app/users/views/profile.html',
              controller: 'ProfileCtrl'
            }
          }
        })
        .state('tab.tabAdverts', {
          url: '/adverts',
          views: {
            'tab-adverts': {
              templateUrl: 'app/adverts/views/adverts-tabs.html'
            }
          }
        })
        .state('tab.tabAdverts.listAdverts', {
          url: '/list-adverts',
          views: {
            'tab-listAdverts': {
              templateUrl: 'app/adverts/views/listAdverts.html',
              controller: 'ListAdvertsCtrl'
            }
          }
        })
        .state('tab.tabAdverts.favorites', {
          url: '/favorites',
          views: {
            'tab-favorites': {
              templateUrl: 'app/adverts/views/listAdverts.html',
              controller: 'FavoritesCtrl'
            }
          }
        })
        .state('tab.tabAdverts.criteria', {
          url: '/criteria',
          views: {
            'tab-criteria': {
              templateUrl: 'app/adverts/views/criteria.html',
              controller: 'CriteriaCtrl'
            }
          }
        })
        .state('tab.menuMessages', {
          url: '/messaging',
          views: {
            'tab-menuMessaging': {
              templateUrl: 'app/messaging/views/side-menu-messaging.html',
              controller: 'MenuMessagingCtrl'
            }
          }
        })
        .state('tab.menuMessages.messages', {
          url: '/messages',
          views: {
            'tab-menuMessaging': {
              templateUrl: 'app/messaging/views/messaging.html',
              controller: 'MessagesCtrl'
            }
          }
        })
        .state('tab.menuMessages.message', {
          url: '/message/:id',
          views: {
            'tab-menuMessaging': {
              templateUrl: 'app/messaging/views/single.html',
              controller: 'MessageCtrl'
            }
          }
        })
        .state('tab.menuMessages.newMessage', {
          url: '/new-message/:owner',
          views: {
            'tab-menuMessaging': {
              templateUrl: 'app/messaging/views/new-message.html',
              controller: 'NewMessageCtrl'
            }
          }
        })

        $urlRouterProvider.otherwise('login');
    }]);
})();
