(function(){
  'use strict';

  angular
    .module('app', [
      'app.adverts',
      'app.core',
      'app.messaging',
      'app.users',
    ])
    .constant('ApiEndpoint', {
      url: 'http://projet5.auroregaucher.com/api'
    })
    .config(function($ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom');
    });
})();
