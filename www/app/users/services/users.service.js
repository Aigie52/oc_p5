angular.module('app.users')
 .factory('UsersService', UsersService);

 function UsersService(Api, $rootScope, $state) {
  return {
    signup: function(user){
      Api.signupWithUsernameAndPassword(angular.toJson(user), function(data) {
        $rootScope.user = data;
        if(data.role === 'owner') $state.go('addAdvert');
        if(data.role === 'adopter') $state.go('tab.profile');
      });
    },
    loginUsername: function(data){
      Api.loginWithUsernameAndPassword(angular.toJson(data), function(user){
        $rootScope.user = user;
        $state.go('tab.home');
      });
    },
    getUserData: function(callback) {
      Api.getApiDataUser($rootScope.user.id, function(user){
          callback(user);
      });
    }
  }
 }
