(function(){
  'use strict';

  angular
    .module('app.users')
    .controller('ProfileCtrl', ProfileCtrl)
    .controller('LoginCtrl', LoginCtrl)
    .controller('SignupCtrl', SignupCtrl);

  ProfileCtrl.$inject = ['$rootScope', '$ionicLoading', 'UsersService'];
  LoginCtrl.$inject = ['$scope', 'UsersService', '$ionicHistory'];
  SignupCtrl.$inject = ['$scope', 'UsersService'];

  function ProfileCtrl($rootScope, $ionicLoading, UsersService){
    $ionicLoading.show();

    UsersService.getUserData(function(user){
      $rootScope.user = user;
      $ionicLoading.hide();
    });

  }

  function LoginCtrl($scope, UsersService, $ionicHistory) {
    $scope.$on('$ionicView.enter', function(event, viewData) {
      $ionicHistory.clearCache();
    });

    $scope.data = {};

    $scope.loginUsername = function(){
        UsersService.loginUsername($scope.data);
    }
  }

  function SignupCtrl($scope, UsersService) {
    $scope.data = {};

    $scope.createUser = function(){
        UsersService.signup($scope.data);
    }
  }
})();
