angular.module('app.messaging')
 .factory('MessagingService', MessagingService);

 function MessagingService(Api, $state) {
  return {
    sendMessage: function(from, to, message){
      var data = {
        from: from,
        to: to,
        message: message
      };
      Api.sendMessage(angular.toJson(data), function(result){
        alert(result.data);
        $state.go('tab.menuMessages.messages');
      });
    },
    getMessages: function(user, callback){
      Api.getMessages(user, function(messages){
        callback(messages);
      });
    },
    getSendMessages: function(user, callback){
      Api.getSendMessages(user, function(messages){
        callback(messages);
      });
    }
  }
 }
