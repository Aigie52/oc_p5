(function(){
  'use strict';

  angular
    .module('app.messaging')
    .controller('MenuMessagingCtrl', MenuMessagingCtrl)
    .controller('MessagesCtrl', MessagesCtrl)
    .controller('MessageCtrl', MessageCtrl)
    .controller('NewMessageCtrl', NewMessageCtrl);

  MenuMessagingCtrl.$inject = ['$scope', 'MessagingService', '$ionicLoading', '$rootScope'];
  MessagesCtrl.$inject = ['$scope', '$rootScope', 'MessagingService', '$ionicLoading'];
  MessageCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$ionicLoading', '$state'];
  NewMessageCtrl.$inject = ['$scope', '$stateParams', '$rootScope', 'MessagingService'];

  function MenuMessagingCtrl($scope, MessagingService, $ionicLoading, $rootScope) {
    $scope.messages = [];
    var user = $rootScope.user.username;
    $ionicLoading.show();

    MessagingService.getMessages(user, function(messages){
      $scope.messages = messages ? messages : [];
      $rootScope.messages = messages;
      MessagingService.getSendMessages(user, function(sendMessages){
        $scope.sendMessages = sendMessages;
        $rootScope.sendMessages = sendMessages;
        $ionicLoading.hide();
      });
    });
  }

  function MessagesCtrl($scope, MessagingService, $ionicLoading){}

  function MessageCtrl($scope, $rootScope, $stateParams, $ionicLoading, $state){
      var id = $stateParams.id;
      var msgs = ($rootScope.messages) ? $rootScope.messages : [];
      var messages = ($rootScope.sendMessages) ? msgs.concat($rootScope.sendMessages) : msgs;

      for(var i = 0; i < messages.length; i++){
        if(messages[i].id === $stateParams.id){
          if(messages[i].from === $rootScope.user.username){
            $scope.currentUserIsSender = true;
          } else {
            $scope.currentUserIsSender = false;
          }
          $scope.message = messages[i];
        }
      };

    $scope.goBack = function() {
      $state.go('tab.menuMessages.messages');
    };
  }

  function NewMessageCtrl($scope, $stateParams, $rootScope, MessagingService){
    $scope.data = {};
    $scope.owner = $stateParams.owner;

    $scope.sendMessage = function() {
      var from = $rootScope.user.username;
      var to = $scope.owner;
      var message = $scope.data.message;

      MessagingService.sendMessage(from, to, message);
    }
  }
})();
