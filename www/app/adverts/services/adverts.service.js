angular.module('app.adverts')
  .factory('AdvertsService', AdvertsService);

  function AdvertsService(Api, $rootScope){
    return {
      saveAdvert: function(data, callback) {
        var newAdvert = {
          lat: data.lat,
          lng: data.lng,
          city: data.city,
          birthDate: data.birthday,
          sex: data.sex,
          vaccinated: data.vaccinated,
          tattooed: data.tattooed,
          description: data.description,
          image: data.image,
          user: $rootScope.user.id
        };
        Api.addNewAdvert(angular.toJson(newAdvert), function(data){
          callback(data);
        })

      },
      getRelevantAdverts : function(data, callback){
        Api.getRelevantAdverts(data, function(adverts) {
          callback(adverts);
        })
      },
      addToFavorites: function(advertId, callback) {
        var user = $rootScope.user.id;
        var data = {
          advert_id: advertId,
          user_id: user
        };
        Api.addToFavorites(angular.toJson(data), function(result){
          alert(result.data);
        });
      },
      getFavoriteAdverts: function(callback) {
        var user = $rootScope.user.id;
        Api.getFavoriteAdverts(user, function(adverts){
          callback(adverts);
        });
      }
    };
  }
