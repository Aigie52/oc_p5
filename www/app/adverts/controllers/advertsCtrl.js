(function(){
  'use strict';

  angular
    .module('app.adverts')
    .controller('AddAdvertCtrl', AddAdvertCtrl)
    .controller('ListAdvertsCtrl', ListAdvertsCtrl)
    .controller('CriteriaCtrl', CriteriaCtrl)
    .controller('FavoritesCtrl', FavoritesCtrl);

  AddAdvertCtrl.$inject = ['$rootScope', '$scope', '$ionicLoading', '$ionicPlatform', '$cordovaCamera', '$cordovaImagePicker', '$cordovaGeolocation', 'AdvertsService', '$cordovaFileTransfer', '$cordovaFile', '$cordovaDevice', '$ionicPopup', '$state'];
  ListAdvertsCtrl.$inject = ['$rootScope', '$scope', '$ionicLoading', 'AdvertsService', '$ionicHistory', '$ionicPlatform', '$state'];
  CriteriaCtrl.$inject = ['$rootScope', '$scope', '$ionicLoading', '$cordovaGeolocation', '$ionicPlatform'];
  FavoritesCtrl.$inject = ['$scope', '$ionicLoading', 'AdvertsService'];


  function AddAdvertCtrl($rootScope, $scope, $ionicLoading, $ionicPlatform, $cordovaCamera, $cordovaImagePicker, $cordovaGeolocation, AdvertsService, $cordovaFileTransfer, $cordovaFile, $cordovaDevice, $ionicPopup, $state) {
    $scope.data = {};
    $scope.image = null;
    $scope.location = {};

    // Geolocation
    $ionicPlatform.ready(function(){
      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Récupération de votre position !'
      });

      var posOptions = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      };

      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.data.lat = lat;
        $scope.data.lng = long;

        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(lat, long);
        var request = {
          latLng: latlng
        };

        geocoder.geocode(request, function(data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[0] != null) {
              var data = data[0].address_components;
              for(var i =0; i < data.length; i++) {
                if(data[i].types[0] === "locality"){
                  $scope.location.city = data[i].long_name;
                }
                if(data[i].types[0] === "postal_code"){
                  $scope.location.postalCode = data[i].long_name;
                }
              };
              $scope.position = data[0].formatted_address;
            } else {
              alert("No address available");
            }
          }
          $ionicLoading.hide();
        });
      }, function(err) {
          $ionicLoading.hide();
          console.log(err);
      });
    });

    // Take picture
    $scope.takePicture = function() {
      var options = {
        quality: 75, // Qualité de l'image sauvée, valeur entre 0 et 100
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG, // Format d'encodage : JPEG ou PNG
        targetWidth: 300, // Largeur de l'image en pixel
        targetHeight: 450, // Hauteur de l'image en pixel
        saveToPhotoAlbum: false // Enregistrer l'image dans l'album photo du device
      };

      $ionicPlatform.ready(function() {
        $cordovaCamera.getPicture(options).then(function(imagePath) {
          var currentName = imagePath.replace(/^.*[\\\/]/, '');

          //Create a new name for the photo
          var d = new Date(),
              n = d.getTime(),
              newFileName =  n + ".jpg";

          var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

          // Move the file to permanent storage
          $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
            $scope.image = newFileName;
          }, function(error){
            alert('Error: ' + JSON.stringify(error));
          });
        }, function(error){
          alert(error);
        });
      });
    };

    $scope.uploadPicture = function(){
      var options = {
        quality: 75, // Qualité de l'image sauvée, valeur entre 0 et 100
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG, // Format d'encodage : JPEG ou PNG
        targetWidth: 300, // Largeur de l'image en pixel
        targetHeight: 450, // Hauteur de l'image en pixel
        saveToPhotoAlbum: false // Enregistrer l'image dans l'album photo du device
      };

      $ionicPlatform.ready(function() {
        $cordovaCamera.getPicture(options).then(function(imagePath) {
          var currentName = imagePath.replace(/^.*[\\\/]/, '');

          //Create a new name for the photo
          var d = new Date(),
              n = d.getTime(),
              newFileName =  n + ".jpg";
          if ($cordovaDevice.getPlatform() == 'Android') {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
              window.resolveLocalFileSystemURL(entry, success, fail);
              function fail(e) {
                alert(e);
              }

              function success(fileEntry) {
                var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                // Only copy because of access rights
                $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                  $scope.image = newFileName;
                }, function(error){
                  alert(error);
                });
              };
            });
          } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
              $scope.image = newFileName;
            }, function(error){
              alert(error);
            });
          }
        }, function(error) {
          alert(error);
        });
      });
    };

    $scope.pathForImage = function(image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };

    $scope.addAdvert = function(){
      $ionicLoading.show();
      var url = "http://projet5.auroregaucher.com/api/upload";

      // File for Upload
      var targetPath = $scope.pathForImage($scope.image);

      // File name only
      var filename = $scope.image;

      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'fileName': filename}
      };

      $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
        $scope.data.image = result.response;
        $scope.data.city = $scope.location.city;

        AdvertsService.saveAdvert($scope.data, function(advert){
          $ionicLoading.hide();
          $state.go('tab.home');
        });
      }, function (error) {
        alert(JSON.stringify(error));
      });
    };
  }

  function ListAdvertsCtrl($rootScope, $scope, $ionicLoading, AdvertsService, $ionicHistory, $ionicPlatform, $state) {

    $scope.$on('$ionicView.enter', function(event, viewData) {
      $ionicHistory.clearCache();

      $scope.title = "Annonces";

      if($rootScope.criteria) {
        var data = $rootScope.criteria;
      } else {
        var data = {};
      }
      $scope.noCards = true;
      $scope.cards = [];
      $ionicLoading.show();

      AdvertsService.getRelevantAdverts(data, function(adverts) {
        if(adverts && adverts.length !== 0) {
          $scope.cards = adverts;
          $scope.noCards = false;
          $ionicLoading.hide();
        } else if(!adverts || adverts.length === 0) {
          $scope.noCards = true;
        }
      });
    });

    $scope.cardDestroyed = function(index) {
      $scope.cards.splice(index, 1);
      if($scope.cards.length === 0) $scope.noCards = true;
    };

    $scope.cardSwipedLeft = function(index) {
      AdvertsService.addToFavorites(index);
      if($scope.cards.length === 0) $scope.noCards = true;
    };

    $scope.cardSwipedRight = function(index) {
      $scope.cards.splice(index, 1);
      if($scope.cards.length === 0) $scope.noCards = true;
    };
  }

  function CriteriaCtrl($rootScope, $scope, $ionicLoading, $cordovaGeolocation, $ionicPlatform) {
    if($rootScope.criteria) {
      $scope.criteria = $rootScope.criteria;
    } else {
      $scope.criteria = {};
      $rootScope.criteria = {};
    }

    $scope.location = {};

    // Geolocation
    $ionicPlatform.ready(function(){
      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Récupération de votre position !'
      });

      var posOptions = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0
      };

      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.criteria.lat = lat;
        $rootScope.criteria.lat = lat;
        $scope.criteria.lng = long;
        $rootScope.criteria.lng = long;

        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(lat, long);
        var request = {
          latLng: latlng
        };

        geocoder.geocode(request, function(data, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (data[0] != null) {
                var data = data[0].address_components;
                for(var i =0; i < data.length; i++) {
                  if(data[i].types[0] === "locality"){
                    $scope.location.city = data[i].long_name;
                  }
                  if(data[i].types[0] === "postal_code"){
                    $scope.location.postalCode = data[i].long_name;
                  }
                };
                $scope.position = data[0].formatted_address;
              } else {
                alert("No address available");
              }
            }
            $ionicLoading.hide();
          });
      }, function(err) {
          $ionicLoading.hide();
          console.log(err);
      });
    });

    $scope.$watch('criteria.distance', function() {
      if($scope.criteria.distance) $rootScope.criteria.distance = $scope.criteria.distance;
    });
    $scope.$watch('criteria.ageMax', function() {
      if($scope.criteria.ageMax) $rootScope.criteria.ageMax = $scope.criteria.ageMax;
    });
    $scope.$watch('criteria.sex', function() {
      if($scope.criteria.sex) $rootScope.criteria.sex = $scope.criteria.sex;
    });
    $scope.$watch('criteria.tattooed', function() {
      if($scope.criteria.tattooed) $rootScope.criteria.tattooed = $scope.criteria.tattooed;
    });
    $scope.$watch('criteria.vaccinated', function() {
      if($scope.criteria.vaccinated) $rootScope.criteria.vaccinated = $scope.criteria.vaccinated;
    });
  }

  function FavoritesCtrl($scope, $ionicLoading, AdvertsService) {
    $scope.title = "Favoris";

    $scope.cards = [];
    $scope.noCards = false;
    $ionicLoading.show();
    //TODO actions swipe
    AdvertsService.getFavoriteAdverts(function(adverts) {
      if(adverts && adverts.length !== 0) {
        $scope.cards = adverts;
        $scope.noCards = false;
        $ionicLoading.hide();
      } else if(!adverts || adverts.length === 0) {
        $scope.noCards = true;
      }
    });
  }
})();
