(function(){
  'use strict';

  angular
    .module('app.adverts', [
      'ionic',
      'ngCordova',
      'ionic.contrib.ui.tinderCards',
    ]);
})();
